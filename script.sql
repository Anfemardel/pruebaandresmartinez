CREATE DATABASE pruebaandresmartinez
USE pruebaandresmartinez
CREATE TABLE vehiculo(
id INT IDENTITY(1,1) PRIMARY KEY,
placa VARCHAR(50),
marca VARCHAR(50),
modelo VARCHAR(50)
)
CREATE TABLE conductor(
id INT IDENTITY(1,1) PRIMARY KEY,
nombre VARCHAR(50),
identificacion INT,
id_vehiculo INT FOREIGN KEY REFERENCES vehiculo(id)
)